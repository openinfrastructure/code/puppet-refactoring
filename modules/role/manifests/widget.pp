class role::widget {

  if $::widget_canary {
    $repo = 'widget-canary'
  } else {
    $repo = 'widget'
  }

  include ::widget::repo
  include ::widget::reload
  include ::widget::syslog

  $legacy_whitelist = [
    'blue',
    'red',
  ]

  $whitelist = [
    'pos',
    'check',
    'change',
    'spend',
    'open',
    'windows',
  ]

  $categories_hash_dc1 = {
    'pos'                 => 'widget-tier3',
    'check'               => 'widget-tier1',
    'change'              => 'widget-tier3',
    'spend'               => 'widget-tier3',
    'open'                => 'widget-tier1',
    'windows'             => 'widget-tier3',
    'screw'               => 'widget-tier3',
    'bolt'                => 'widget-tier3',
    'nut'                 => 'widget-tier3',
    'gear'                => 'widget-tier3',
    'belt'                => 'widget-tier2',
    'strap'               => 'widget-tier2',
    'floor'               => 'widget-tier2',
    'ceiling'             => 'widget-tier2',
    'corp'                => 'widget-tier2',
    'dmz'                 => 'widget-tier2',
  }

  $categories_hash_dc2 = {
    'pos'                 => 'widget-tier4',
    'check'               => 'widget-tier3',
    'change'              => 'widget-tier1',
    'spend'               => 'widget-tier2',
    'open'                => 'widget-tier4',
    'windows'             => 'widget-tier1',
    'screw'               => 'widget-tier2',
    'bolt'                => 'widget-tier2',
    'nut'                 => 'widget-tier2',
    'gear'                => 'widget-tier2',
    'belt'                => 'widget-tier1',
    'strap'               => 'widget-tier1',
    'floor'               => 'widget-tier1',
    'ceiling'             => 'widget-tier1',
    'corp'                => 'widget-tier1',
    'dmz'                 => 'widget-tier1',
  }

  $important_products_hash_tier3 = {
    'lorem'                                => 'widget-tier3',
    'ipsum'                                => 'widget-tier3',
    'floor'                                => 'widget-tier3',
    'foor'                                 => 'widget-tier3',
    'baar'                                 => 'widget-tier3',
    'baaaz'                                => 'widget-tier3',
    'adhoc'                                => 'widget-tier3',
    'badhoc'                               => 'widget-tier3',
    'madhoc'                               => 'widget-tier3',
    'radhoc'                               => 'widget-tier3',
    'whohoc'                               => 'widget-tier3',
    'youhoc'                               => 'widget-tier3',
    'red'                                  => 'widget-tier3',
    'blue'                                 => 'widget-tier3',
    'orange'                               => 'widget-tier3',
    'green'                                => 'widget-tier3',
    'brown'                                => 'widget-tier3',
    'clear'                                => 'widget-tier3',
    'black'                                => 'widget-tier3',
    'white'                                => 'widget-tier3',
    'black_and_white'                      => 'widget-tier3',
    'red_and_blue'                         => 'widget-tier3',
    'red_then_blue'                        => 'widget-tier3',
    'blue_then_red'                        => 'widget-tier3',
    'green_or_orange'                      => 'widget-tier3',
    'backup_lorem'                         => 'widget-tier3',
    'backup_ipsum'                         => 'widget-tier3',
    'backup_floor'                         => 'widget-tier3',
    'backup_foor'                          => 'widget-tier3',
    'backup_baar'                          => 'widget-tier3',
    'backup_baaaz'                         => 'widget-tier3',
    'backup_adhoc'                         => 'widget-tier3',
    'backup_badhoc'                        => 'widget-tier3',
    'backup_madhoc'                        => 'widget-tier3',
    'backup_radhoc'                        => 'widget-tier3',
    'backup_whohoc'                        => 'widget-tier3',
    'backup_youhoc'                        => 'widget-tier3',
    'backup_red'                           => 'widget-tier3',
    'backup_blue'                          => 'widget-tier3',
    'backup_orange'                        => 'widget-tier3',
    'backup_green'                         => 'widget-tier3',
    'backup_brown'                         => 'widget-tier3',
    'backup_clear'                         => 'widget-tier3',
    'backup_black'                         => 'widget-tier3',
    'backup_white'                         => 'widget-tier3',
    'backup_black_and_white'               => 'widget-tier3',
    'backup_red_and_blue'                  => 'widget-tier3',
    'backup_red_then_blue'                 => 'widget-tier3',
    'backup_blue_then_red'                 => 'widget-tier3',
    'backup_green_or_orange'               => 'widget-tier3',
    'backup_green_and_orange_but_not_red'  => 'widget-tier3',
    'backup_green_and_orange_but_not_red'  => 'widget-tier3',
    'standby_lorem'                        => 'widget-tier3',
    'standby_ipsum'                        => 'widget-tier3',
    'standby_floor'                        => 'widget-tier3',
    'standby_foor'                         => 'widget-tier3',
    'standby_baar'                         => 'widget-tier3',
    'standby_baaaz'                        => 'widget-tier3',
    'standby_adhoc'                        => 'widget-tier3',
    'standby_badhoc'                       => 'widget-tier3',
    'standby_madhoc'                       => 'widget-tier3',
    'standby_radhoc'                       => 'widget-tier3',
    'standby_whohoc'                       => 'widget-tier3',
    'standby_youhoc'                       => 'widget-tier3',
    'standby_red'                          => 'widget-tier3',
    'standby_blue'                         => 'widget-tier3',
    'standby_orange'                       => 'widget-tier3',
    'standby_green'                        => 'widget-tier3',
    'standby_brown'                        => 'widget-tier3',
    'standby_clear'                        => 'widget-tier3',
    'standby_black'                        => 'widget-tier3',
    'standby_white'                        => 'widget-tier3',
    'standby_black_and_white'              => 'widget-tier3',
    'standby_red_and_blue'                 => 'widget-tier3',
    'standby_red_then_blue'                => 'widget-tier3',
    'standby_blue_then_red'                => 'widget-tier3',
    'standby_green_or_orange'              => 'widget-tier3',
    'standby_green_and_orange_but_not_red' => 'widget-tier3',
    'offsite_lorem'                        => 'widget-tier3',
    'offsite_ipsum'                        => 'widget-tier3',
    'offsite_floor'                        => 'widget-tier3',
    'offsite_foor'                         => 'widget-tier3',
    'offsite_baar'                         => 'widget-tier3',
    'offsite_baaaz'                        => 'widget-tier3',
    'offsite_adhoc'                        => 'widget-tier3',
    'offsite_badhoc'                       => 'widget-tier3',
    'offsite_madhoc'                       => 'widget-tier3',
    'offsite_radhoc'                       => 'widget-tier3',
    'offsite_whohoc'                       => 'widget-tier3',
    'offsite_youhoc'                       => 'widget-tier3',
    'offsite_red'                          => 'widget-tier3',
    'offsite_blue'                         => 'widget-tier3',
    'offsite_orange'                       => 'widget-tier3',
    'offsite_green'                        => 'widget-tier3',
    'offsite_brown'                        => 'widget-tier3',
    'offsite_clear'                        => 'widget-tier3',
    'offsite_black'                        => 'widget-tier3',
    'offsite_white'                        => 'widget-tier3',
    'offsite_black_and_white'              => 'widget-tier3',
    'offsite_red_and_blue'                 => 'widget-tier3',
    'offsite_red_then_blue'                => 'widget-tier3',
    'offsite_blue_then_red'                => 'widget-tier3',
    'offsite_green_or_orange'              => 'widget-tier3',
    'offsite_green_and_orange_but_not_red' => 'widget-tier3',
  }

  service { 'widget':
    enable => true,
  }

  $initfile = 'puppet:///modules/widget/widgetd-init'

  if $::puppet_role == 'widget.public' {
    file {
      '/data/widget':
        ensure => directory,
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
    }
  }
  else {
    file {
      '/var/spool/widget':
        ensure => directory,
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
    }
  }

  file {
    '/var/log/widget':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755';

    '/var/log/widget/widgetmon':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755';

    '/usr/sbin/widget_tail':
      ensure  => file,
      content => template('widget/widget_tail.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0755';

    '/usr/sbin/widget_ctrl':
      ensure  => file,
      content => template('widget/widget_ctrl.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0755';

    '/etc/init.d/widget':
      ensure  => file,
      source  => $initfile,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      require => Package['widget-package'];

    '/etc/widget.d':
      ensure  => directory,
      recurse => true,
      purge   => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0755';
  }

  ::cron::job { 'widget_cleanup_var_spool_widget':
    command => 'find /var/spool/widget -type f -mtime +5 -delete || true',
    hour    => fqdn_rand(24),
    minute  => fqdn_rand(60);
  }

  ::cron::job { 'widget_cleanup_data_widget':
    command => 'find /data/widget -type f -mtime +5 -delete || true',
    hour    => fqdn_rand(24),
    minute  => fqdn_rand(60);
  }

  ::cron::job { 'widget_cleanup_data_disk1_widget_spool':
    command => 'find /data/disk1/widget_spool -type f -mtime +7 -delete || true',
    hour    => fqdn_rand(24),
    minute  => fqdn_rand(60);
  }

  include widget::install
  include widget::config

}
