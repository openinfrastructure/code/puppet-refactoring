class role::zookeeper::corp {

  case $::datacenter {
    'a1', 'a2', 'b1', 'b2': {

      $cluster_name = "corp-zookeeper@${::datacenter}"
      $roles = ['primary', 'canary']
      $owners = 'corp'


      case $::datacenter {
        'a1': {
          zookeeper::clusters::a1::install {'1.0.0':}
        }
        'a2': {
          zookeeper::clusters::a2::install {'1.0.0':}
          # (ZOO-5678) user was missing for some reason.
          user { 'zookeeper':
            ensure => present,
          }
        }
        'b1': {
          zookeeper::clusters::b1::install {'1.2.0':}
        }
        'b2': {
          zookeeper::clusters::b2::install {'1.2.0':}
        }
      }

      include ::zookeeper::tools::monitoring
      include ::zookeeper::tools::debugging
      include ::zookeeper::tools::secrets
      include ::zookeeper::tools::tuning
    }
  }

  cron::job { 'delete_dumps':
    command => "find / -maxdepth 1 -type f -mtime +7 -name \"mem_dump*\" | xargs rm -f ",
    user    => root,
    minute  => fqdn_rand(10),
    hour    => 8,
  }

  logrotate::rule { 'messages':
    path => '/var/log/messages',
  }

}
