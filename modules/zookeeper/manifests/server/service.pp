class zookeeper::server::service {

  service { 'zookeeper':
    ensure  => 'running',
    enable  => false,
    require => Exec['systemctl-reload'],
  }

}
