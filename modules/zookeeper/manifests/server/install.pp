class zookeeper::server::install {

  $version = $::datacenter ? {
    'a'       => '1.0.0',
    'b'       => '1.2.0',
    'c'       => 'latest',
    'default' => '1.1.0',
  }

  if (versioncmp($facts['os']['release']['major'], '6') > 0) {
    package { 'zookeeper':
      ensure => $version,
    }
  } else {
    package { 'zookeeperd':
      ensure => $version,
    }
  }

}
