class zookeeper::server::config (
  $allow_auth = 'nate',
  $listen_address = '127.0.0.1',
) {

  file { '/etc/zookeeper/zoo.conf':
    ensure => file,
    owner  => $::puppet_role ? {
      /zoo/   => 'zookeeper',
      default => 'root',
    },
    group  => $::puppet_role ? {
      /zoo/   => 'zookeeper',
      default => 'root',
    },
    mode   => '0440',
  }

  ini_setting { 'zookeeper-auth':
    path    => '/etc/zookeeper/zoo.conf',
    setting => 'allow-auth',
    value   => $allow_auth,
  }

  ini_setting { 'zookeeper-listen-address':
    path    => '/etc/zookeeper/zoo.conf',
    setting => 'listen-address',
    value   => $listen_address,
  }

}
