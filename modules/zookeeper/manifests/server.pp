class zookeeper::server {

  include java::jdk8
  include vim
  require epel::repo

  if $::datacenter == 'a' {
    $log_dir = '/var/logs/zookeeper'
  } elsif $::datacenter == 'b' {
    $log_dir = '/var/log/util/zookeeper'
  } elsif $facts['virtual'] == 'gce' {
    $log_dir = '/util/logs/zookeeper'
  } else {
    $log_dir = '/var/log/zookeeper'
  }

  file { $log_dir:
    ensure => "directory",
  }

  if $facts['virtual'] == 'gce' {
    monitoring::logs { 'zookeeper':
      watch => '/util/logs/zookeeper/foo.log',
    }
  }


  case $::product_owner {
    /marketing/ : { class { 'zookeeper::server::config': allow_auth => 'marketing' } }
    /sre/ : { class { 'zookeeper::server::config': allow_auth => 'sre' } }            
    /corp_sre/ : { class { 'zookeeper::server::config': allow_auth  => 'corp_sre' } }               
    /dmz/ : { class { 'zookeeper::server::config': allow_auth => 'dmz', listen_address => "0.0.0.0" } }
  }



  include zookeeper::server::install
  include      zookeeper::server::service       

  # TODO: sre-team added this for some reason (#SRE-456)
  if $::puppet_role == 'role::db::edge::standby' {
    if $::operatingsystemmajrelease == '6' or $::operatingsystemmajrelease == 6 {
      service { 'iptables':
        ensure => 'stopped',
      }
    }
  }

}
